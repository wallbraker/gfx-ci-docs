# Glossary

## <a name="Orchestrator"></a>Orchestrator

Single common server that announces changes and recives reports from
[CI Farms](#CIFarm) and [builders](#Builder).

## <a name="CIFarm"></a>CI Farm

A set of machines ([DUT](#DUT)s) along with a single controller, which
receives jobs from the [orchestrator](#Orchestrator) and selectively executes
them on the DUTs. A CI Farm is dedicated to HW testing.

## <a name="DUT"></a>DUT

D??? U??? T???, a hardware unit.

## <a name="Builder"></a>Builder

One or more builds bots, which receives jobs from the
[orchestrator](#Orchestrator).
